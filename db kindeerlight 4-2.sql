-- phpMyAdmin SQL Dump
-- version 5.0.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Waktu pembuatan: 03 Feb 2021 pada 18.15
-- Versi server: 10.4.17-MariaDB
-- Versi PHP: 8.0.0

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `kindeerlight`
--
DROP DATABASE IF EXISTS `kindeerlight`;
CREATE DATABASE IF NOT EXISTS `kindeerlight` DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci;
USE `kindeerlight`;

-- --------------------------------------------------------

--
-- Struktur dari tabel `tbl_admin`
--
-- Pembuatan: 09 Jan 2021 pada 07.37
--

DROP TABLE IF EXISTS `tbl_admin`;
CREATE TABLE `tbl_admin` (
  `id` int(11) NOT NULL,
  `name` varchar(50) NOT NULL,
  `email` varchar(25) NOT NULL,
  `password` varchar(255) NOT NULL,
  `role` int(11) NOT NULL DEFAULT 0 COMMENT '0 : admin | 1 : superadmin',
  `created_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `last_login` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- RELATIONSHIPS FOR TABLE `tbl_admin`:
--

-- --------------------------------------------------------

--
-- Struktur dari tabel `tbl_alamat`
--
-- Pembuatan: 30 Jan 2021 pada 15.40
--

DROP TABLE IF EXISTS `tbl_alamat`;
CREATE TABLE `tbl_alamat` (
  `id` int(11) NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  `title` varchar(25) NOT NULL,
  `name` varchar(25) NOT NULL,
  `phone` varchar(15) NOT NULL,
  `street` varchar(255) NOT NULL,
  `province` int(11) NOT NULL,
  `city` int(11) NOT NULL,
  `district` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- RELATIONSHIPS FOR TABLE `tbl_alamat`:
--   `user_id`
--       `tbl_user` -> `id`
--

-- --------------------------------------------------------

--
-- Struktur dari tabel `tbl_kategori`
--
-- Pembuatan: 13 Jan 2021 pada 08.18
--

DROP TABLE IF EXISTS `tbl_kategori`;
CREATE TABLE `tbl_kategori` (
  `id` int(11) NOT NULL,
  `name` varchar(25) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- RELATIONSHIPS FOR TABLE `tbl_kategori`:
--

--
-- Dumping data untuk tabel `tbl_kategori`
--

INSERT INTO `tbl_kategori` (`id`, `name`, `created_at`, `updated_at`) VALUES
(1, 'Scented Candles', '2021-01-13 08:36:37', '2021-01-13 08:36:37'),
(2, 'Reed Diffuser', '2021-01-13 08:36:37', '2021-01-13 08:36:37'),
(3, 'Air Freshner', '2021-01-13 08:37:27', '2021-01-13 08:37:27'),
(4, 'Gift', '2021-01-13 08:37:27', '2021-01-13 08:37:27');

-- --------------------------------------------------------

--
-- Struktur dari tabel `tbl_product`
--
-- Pembuatan: 21 Jan 2021 pada 13.31
--

DROP TABLE IF EXISTS `tbl_product`;
CREATE TABLE `tbl_product` (
  `id` int(11) NOT NULL,
  `name` varchar(50) NOT NULL,
  `description` varchar(255) NOT NULL,
  `kategori_id` int(11) NOT NULL,
  `price` float NOT NULL DEFAULT 1,
  `stock` int(11) NOT NULL DEFAULT 1,
  `weight` int(11) NOT NULL DEFAULT 0,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- RELATIONSHIPS FOR TABLE `tbl_product`:
--   `kategori_id`
--       `tbl_kategori` -> `id`
--

--
-- Dumping data untuk tabel `tbl_product`
--

INSERT INTO `tbl_product` (`id`, `name`, `description`, `kategori_id`, `price`, `stock`, `weight`, `created_at`, `updated_at`) VALUES
(1, 'Signature Jar', 'Signture Jar 200gram', 1, 82000, 1, 500, '2021-01-11 10:28:48', '2021-01-11 10:28:48'),
(2, 'Signature Glass', 'Signature Glass 70gram', 1, 39000, 1, 250, '2021-01-11 10:28:48', '2021-01-11 10:28:48'),
(3, 'Wax Sachet', 'Wax Sachet / Airfreshner', 3, 22000, 1, 100, '2021-01-11 10:30:00', '2021-01-11 10:30:00'),
(4, 'Exclusive Gift Package', 'Custom Candle', 1, 62000, 1, 250, '2021-01-11 10:30:00', '2021-01-11 10:30:00'),
(5, 'Gift Card', 'Exclusive Gift Card', 4, 3200, 1, 10, '2021-01-12 23:39:17', '2021-01-12 23:39:17'),
(6, 'Paket Hemat Signature Glass & Jar', 'Bundle paket hemat size glass dan jar', 4, 121000, 1, 750, '2021-01-12 23:39:17', '2021-01-12 23:39:17'),
(7, 'Gift Set A', 'Paket Gift Set : Signature Glass 3', 4, 117000, 1, 750, '2021-01-12 23:41:34', '2021-01-12 23:41:34'),
(8, 'Gift Set B', 'In box :\r\n- Signature Glass \r\n- Wax Sachet\r\n- Custom candle', 4, 123000, 1, 750, '2021-01-12 23:41:34', '2021-01-12 23:41:34'),
(9, 'Gift Set C', 'In box :\r\n- Signature Jar\r\n- Signature Glass x 2', 4, 160000, 1, 1000, '2021-01-12 23:43:23', '2021-01-12 23:43:23'),
(10, 'Gift Set D', 'In box :\r\n- Signature Jar x 2\r\n- Wax Sachet x 2', 4, 208000, 1, 1250, '2021-01-12 23:43:23', '2021-01-12 23:43:23');

-- --------------------------------------------------------

--
-- Struktur dari tabel `tbl_product_pict`
--
-- Pembuatan: 13 Jan 2021 pada 08.04
--

DROP TABLE IF EXISTS `tbl_product_pict`;
CREATE TABLE `tbl_product_pict` (
  `id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `url` varchar(255) NOT NULL,
  `isMain` tinyint(1) NOT NULL DEFAULT 0,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- RELATIONSHIPS FOR TABLE `tbl_product_pict`:
--   `product_id`
--       `tbl_product` -> `id`
--

--
-- Dumping data untuk tabel `tbl_product_pict`
--

INSERT INTO `tbl_product_pict` (`id`, `product_id`, `url`, `isMain`, `created_at`, `updated_at`) VALUES
(1, 1, 'signature-jar-1.jpg', 1, '2021-01-21 09:49:58', '2021-01-21 09:49:58'),
(2, 1, 'signature-jar-2.jpg', 0, '2021-01-21 09:49:58', '2021-01-21 09:49:58'),
(3, 2, 'signature-glass-1.jpg', 1, '2021-01-21 09:50:34', '2021-01-21 09:50:34'),
(4, 2, 'signature-glass-2.jpg', 0, '2021-01-21 09:50:34', '2021-01-21 09:50:34'),
(5, 2, 'signature-glass-3.jpg', 0, '2021-01-21 09:51:03', '2021-01-21 09:51:03'),
(6, 4, 'Exclusive-gift-pack.jpg', 1, '2021-01-21 09:51:03', '2021-01-21 09:51:03'),
(7, 7, 'Untitled-1-01.jpg', 1, '2021-01-21 09:52:26', '2021-01-21 09:52:26'),
(8, 8, 'Untitled-2.jpg', 1, '2021-01-21 09:52:26', '2021-01-21 09:52:26'),
(9, 9, 'Untitled-2.jpg', 1, '2021-01-21 09:53:36', '2021-01-21 09:53:36'),
(10, 10, 'Untitled-2.jpg', 1, '2021-01-21 09:53:36', '2021-01-21 09:53:36');

-- --------------------------------------------------------

--
-- Struktur dari tabel `tbl_testimoni`
--
-- Pembuatan: 11 Jan 2021 pada 09.10
--

DROP TABLE IF EXISTS `tbl_testimoni`;
CREATE TABLE `tbl_testimoni` (
  `id` int(11) NOT NULL,
  `name` varchar(25) NOT NULL,
  `testi` varchar(255) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- RELATIONSHIPS FOR TABLE `tbl_testimoni`:
--

--
-- Dumping data untuk tabel `tbl_testimoni`
--

INSERT INTO `tbl_testimoni` (`id`, `name`, `testi`, `created_at`, `updated_at`) VALUES
(1, 'Diandra', 'Packaging sangat bagus, simple dan aman. Admin fast respon, ramah dan informatif sehingga tidak mengecewakan saat membeli. Sudah bagus, bisa tambah greeting card. Kedepannya mungkin produk bisa ditambah dengan korek api kayu jadi semakin lucu dan efisien.', '2021-01-11 09:13:41', '2021-01-11 09:13:41'),
(2, 'Endah', 'Baunya relaxing+wangi, excited buat nyobain scented candle yg agak berbeda dari lainnya.', '2021-01-11 09:13:41', '2021-01-11 09:13:41'),
(3, 'Adeline VK', 'Packing bubble wrap bisa ditambah 1 putaran lagi, supaya lebih aman..', '2021-01-11 09:15:34', '2021-01-11 09:15:34'),
(4, 'Valenzia Tiffani Darmawan', 'Wangi bangettt sukaa, rapi tulisan custom nyaa, mamah aku suka bgt makasihh yaa kak💚', '2021-01-11 09:15:34', '2021-01-11 09:15:34'),
(5, 'Margareta Widya M', 'Belom aku pake tapi lucu banget, cocok buat kado ultah.', '2021-01-11 09:16:58', '2021-01-11 09:16:58');

-- --------------------------------------------------------

--
-- Struktur dari tabel `tbl_user`
--
-- Pembuatan: 09 Jan 2021 pada 07.37
--

DROP TABLE IF EXISTS `tbl_user`;
CREATE TABLE `tbl_user` (
  `id` int(11) NOT NULL,
  `name` varchar(50) NOT NULL,
  `email` varchar(25) NOT NULL,
  `phone` varchar(15) NOT NULL,
  `password` varchar(255) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `last_login` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- RELATIONSHIPS FOR TABLE `tbl_user`:
--

-- --------------------------------------------------------

--
-- Struktur dari tabel `tbl_voucher`
--
-- Pembuatan: 30 Jan 2021 pada 16.09
--

DROP TABLE IF EXISTS `tbl_voucher`;
CREATE TABLE `tbl_voucher` (
  `id` int(11) NOT NULL,
  `name` varchar(50) NOT NULL,
  `type` varchar(20) NOT NULL,
  `value` int(11) NOT NULL,
  `code` varchar(10) NOT NULL,
  `limit_stock` int(11) NOT NULL,
  `min_order` float NOT NULL,
  `max_disc` float NOT NULL,
  `date_start` datetime NOT NULL,
  `date_end` datetime NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- RELATIONSHIPS FOR TABLE `tbl_voucher`:
--

--
-- Indexes for dumped tables
--

--
-- Indeks untuk tabel `tbl_admin`
--
ALTER TABLE `tbl_admin`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `email` (`email`);

--
-- Indeks untuk tabel `tbl_alamat`
--
ALTER TABLE `tbl_alamat`
  ADD PRIMARY KEY (`id`),
  ADD KEY `user_id` (`user_id`);

--
-- Indeks untuk tabel `tbl_kategori`
--
ALTER TABLE `tbl_kategori`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `tbl_product`
--
ALTER TABLE `tbl_product`
  ADD PRIMARY KEY (`id`),
  ADD KEY `kategori_id` (`kategori_id`);

--
-- Indeks untuk tabel `tbl_product_pict`
--
ALTER TABLE `tbl_product_pict`
  ADD PRIMARY KEY (`id`),
  ADD KEY `product_id` (`product_id`);

--
-- Indeks untuk tabel `tbl_testimoni`
--
ALTER TABLE `tbl_testimoni`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `tbl_user`
--
ALTER TABLE `tbl_user`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `email` (`email`);

--
-- AUTO_INCREMENT untuk tabel yang dibuang
--

--
-- AUTO_INCREMENT untuk tabel `tbl_admin`
--
ALTER TABLE `tbl_admin`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT untuk tabel `tbl_alamat`
--
ALTER TABLE `tbl_alamat`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT untuk tabel `tbl_kategori`
--
ALTER TABLE `tbl_kategori`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT untuk tabel `tbl_product`
--
ALTER TABLE `tbl_product`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT untuk tabel `tbl_product_pict`
--
ALTER TABLE `tbl_product_pict`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT untuk tabel `tbl_testimoni`
--
ALTER TABLE `tbl_testimoni`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT untuk tabel `tbl_user`
--
ALTER TABLE `tbl_user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- Ketidakleluasaan untuk tabel pelimpahan (Dumped Tables)
--

--
-- Ketidakleluasaan untuk tabel `tbl_alamat`
--
ALTER TABLE `tbl_alamat`
  ADD CONSTRAINT `tbl_alamat_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `tbl_user` (`id`) ON UPDATE CASCADE;

--
-- Ketidakleluasaan untuk tabel `tbl_product`
--
ALTER TABLE `tbl_product`
  ADD CONSTRAINT `tbl_product_ibfk_1` FOREIGN KEY (`kategori_id`) REFERENCES `tbl_kategori` (`id`) ON UPDATE CASCADE;

--
-- Ketidakleluasaan untuk tabel `tbl_product_pict`
--
ALTER TABLE `tbl_product_pict`
  ADD CONSTRAINT `tbl_product_pict_ibfk_1` FOREIGN KEY (`product_id`) REFERENCES `tbl_product` (`id`) ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
