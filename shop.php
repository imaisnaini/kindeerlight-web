<!-- ================================================
    * Shop Page Kindeerlight
    * Written by imaisnaini
    * Created at 8th January 2021    
    ================================================ -->
<?php
    $page = 'shop';
  
    include 'admin/core/init.php';
    include 'include/header.php';
    include 'include/navbar.php';
    session_start();
    if(!isset($_SESSION['login_user'])) {
        //header('location:login/index.php');
    }else {
        $login_user = $_SESSION['login_user'];
    }
?>

<body data-spy="scroll" data-target="#navbar-example">
<div class="area-padding mt-3">
    <div class="container">    
        <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="section-headline text-center">
                    <h2>Shop</h2>
                </div>
            </div>
        </div>
        <div class="row">
        <?php
          $queryProduct = "SELECT a.id, a.name, a.price, b.url FROM tbl_product a, tbl_product_pict b 
                          WHERE b.product_id = a.id AND b.isMain = 1";
          $resultProduct = mysqli_query($conn, $queryProduct);
          while($rowProduct = $resultProduct->fetch_assoc()){
        ?>
        <div class="col-md-3 col-sm-4 col-xs-6 mt-3">
          <div class="single-team-member">
            <div class="team-img">
              <a href="#">          
                <?php echo '<img src="assets/img/products/'.$rowProduct['url'].'" alt="'.$rowProduct['url'].'">';?>
              </a>
              <div class="team-social-icon text-center">
                <ul>
                  <li>
                    <a href="#">
                      <i class="fa fa-shopping-cart"></i>
                    </a>
                  </li>
                </ul>
              </div>
            </div>
            <div class="team-content text-center">
              <p><?php echo $rowProduct['name']; ?></p>
              <p>Rp. <?php echo number_format($rowProduct['price']);?></p>
            </div>
          </div>
        </div>
        <?php } ?>
        </div>
    </div>
</div>
</body>

<?php
	include 'include/footer.php'
?>
</html>