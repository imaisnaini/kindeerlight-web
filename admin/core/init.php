<!-- ================================================
    * Website Kindeerlight
    * Written by imaisnaini
    * Created at 11th January 2021    
    * This file for configuration database
    ================================================ -->
<?php 
    $servername = "localhost";
    $username = "root";
    $password = "";
    $dbname = "kindeerlight";

    // Create connection
    $conn = new mysqli($servername, $username, $password, $dbname);

    // Check connection
    if ($conn->connect_error) {
        echo "Databaase connection failed with followings errors : ". $conn->connect_error;
        die();
    }
 ?>