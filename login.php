<!-- ================================================
    * Login Page Kindeerlight
    * Written by imaisnaini
    * Created at 21th January 2021    
    ================================================ -->
    <?php
    $page = 'login';
  
    include 'admin/core/init.php';
    include 'include/header.php';
    include 'include/navbar.php';
    session_start();
    if(!isset($_SESSION['login_user'])) {
        //header('location:login/index.php');
    }else {
        $login_user = $_SESSION['login_user'];
    }
?>
<body data-spy="scroll" data-target="#navbar-example">
<div class="area-padding mt-3">
    <div class="container">
      <div class="row">
        <div class="col-md-6 col-sm-12 col-xs-12">
          <h3>Login</h3>
          <div class="form">
            <form action="" method="post">
              <div class="form-group">
                <input type="text" name="email" class="form-control" placeholder="Your Email" data-rule="email" data-msg="Please enter a valid email" />
              </div>
              <div class="form-group mt-3">
                <input type="text" name="password" class="form-control" placeholder="Your Password" data-rule="password" data-msg="Please enter a valid email" />
              </div>
              <div class="text-center"><button type="submit">Send Message</button></div>
            </form>
          </div>
        </div><!-- End Login Div-->
        <div class="col-md-6 col-sm-12 col-xs-12">
          <h3>Register</h3>
          <div class="form">
            <form action="" method="post">
              <div class="form-group">
                <input type="text" name="email" class="form-control" placeholder="Your Email" data-rule="email" data-msg="Please enter a valid email" />
              </div>
              <div class="form-group mt-3">
                <input type="text" name="password" class="form-control" placeholder="Your Password" data-rule="password" data-msg="Please enter a valid email" />
              </div>
              <div class="text-center"><button type="submit">Send Message</button></div>
            </form>
          </div>
        </div><!-- End Resgister Div-->
      </div>
    </div>
</div>
</body>

<?php
	include 'include/footer.php'
?>
</html>