<!-- ================================================
    * Insight Page Kindeerlight
    * Written by imaisnaini
    * Created at 8th January 2021    
    ================================================ -->

<?php
  $page = 'insight';
  
	include 'admin/core/init.php';
	include 'include/header.php';
	include 'include/navbar.php';
	session_start();
    if(!isset($_SESSION['login_user'])) {
      //header('location:login/index.php');
    }else {
      $login_user = $_SESSION['login_user'];
    }
?>

<?php
	include 'include/footer.php'
?>