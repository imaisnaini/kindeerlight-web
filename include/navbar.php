<!-- ================================================
    * Website Kindeerlight
    * Written by imaisnaini
    * Created at 11th January 2021    
    * This file for configuration database
    ================================================ -->
    
<!-- ======= Header ======= -->
<header id="header" class="fixed-top">
  <div class="container d-flex justify-content-between">

    <div class="logo">
      <!-- TODO: ubah logo Kindeerlight-->
      <h1 class="text-light"><a href="index.php"><span>Kindeer</span>light</a></h1>
      <!-- Uncomment below if you prefer to use an image logo -->
      <!-- <a href="index.html"><img src="assets/img/logo.png" alt="" class="img-fluid"></a>-->
    </div>

    <nav class="nav-menu d-none d-lg-block">
      <ul>
        <li <?php if($page=="index")echo 'class="active"';?>><a href="index.php">Home</a></li>
        <li <?php if($page=="shop")echo 'class="active drop-down"';?> class="drop-down"><a href="shop.php">Shop</a>
          <ul>
          <?php 
            $queryKategori = "SELECT * FROM tbl_kategori";
            $resultKategori = mysqli_query($conn, $queryKategori);
            while($rowKategori = $resultKategori->fetch_assoc()){
          ?>
            <li><a href="#"><?php echo $rowKategori['name'];?></a></li>
          <?php } ?>
          </ul>
        </li>
        <li <?php if($page=="insight")echo 'class="active"';?>><a href="insight.php">insight</a></li>
        <li <?php if($page=="contact")echo 'class="active"';?>><a href="contact.php">Contact</a></li>
        <li <?php if($page=="login")echo 'class="active"';?>><a href="login.php">Login</a></li>
      </ul>
    </nav><!-- .nav-menu -->
  </div>
</header>

