<!-- ================================================
    * Website Kindeerlight
    * Written by imaisnaini
    * Created at 11th January 2021    
    * This file for configuration database
    ================================================ -->

  <!-- ======= Footer ======= -->
  <footer>
    <div class="footer-area">
      <div class="container">
        <div class="row">
          <div class="col-md-4">
            <div class="footer-content">
              <div class="footer-head">
                <div class="footer-logo">
                  <h2><span>Kindeer</span>light</h2>
                </div>
              </div>
              <p>Connect with us</p>
              <div class="footer-icons">
                <ul>
                  <li>
                    <a href="https://www.facebook.com/kindeerlight"><i class="fa fa-facebook"></i></a>
                  </li>
                  <li>
                    <a href="https://twitter.com/kindeerlight"><i class="fa fa-twitter"></i></a>
                  </li>
                  <li>
                    <a href="https://www.instagram.com/kindeerlight/"><i class="fa fa-instagram"></i></a>
                  </li>
                </ul>
              </div>
            </div>
          </div>
          <!-- end single fooer -->
          <div class="col-md-4">
            <div class="footer-content">
              <div class="footer-head">
                <h4>Information</h4>
              </div>  
              <p>
              Jl. Kedungpengkol Wetan No 3, Gubeng, Surabaya
              </p>
              <div class="footer-contacts">
                <p><span>Tel:</span> +62822 4544 3941</p>
                <p><span>Email:</span> kindeerlight@gmail.com</p>
                <p><span>Working Hours:</span> 9am-5pm</p>
              </div>            
            </div>
          </div>
          <!-- end single footer -->
          <div class="col-md-4">
            <div class="footer-content footer-newsletter">
              <div class="footer-head">
                <h4>Newsletter</h4>
              </div>          
              <p>Enter your email below to get updates on new scents, sales, and our best deals... for new customers you'll also get a coupon for 10% off your first order!</p>
              <!-- TODO: Fix form style -->
              <form class="form" action="" method="post">
                <input class="form-control"type="email" name="email">
                <input type="submit" value="Subscribe">
              </form>
            </div>
          </div>
        </div>
      </div>
    </div>
    <div class="footer-area-bottom">
      <div class="container">
        <div class="row">
          <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="copyright text-center">
              <p>
                &copy; Copyright <strong>eBusiness</strong>. All Rights Reserved
              </p>
            </div>
            <div class="credits">
              <!--
              All the links in the footer should remain intact.
              You can delete the links only if you purchased the pro version.
              Licensing information: https://bootstrapmade.com/license/
              Purchase the pro version with working PHP/AJAX contact form: https://bootstrapmade.com/buy/?theme=eBusiness
            -->
              Designed by <a href="https://bootstrapmade.com/">BootstrapMade</a>
            </div>
          </div>
        </div>
      </div>
    </div>
  </footer><!-- End  Footer -->

  <!-- ===== Floating Chat Button ===== -->
  <!-- TODO: Work on something here so it wont scroll up & change the collor -->
  <a href="https://wa.me/6281938442591/" class="back-to-top"><i class="fa fa-whatsapp"></i> Chat with us</a>

  <!-- Vendor JS Files -->
  <script src="assets/vendor/jquery/jquery.min.js"></script>
  <script src="assets/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
  <script src="assets/vendor/jquery.easing/jquery.easing.min.js"></script>
  <script src="assets/vendor/php-email-form/validate.js"></script>
  <script src="assets/vendor/appear/jquery.appear.js"></script>
  <script src="assets/vendor/knob/jquery.knob.js"></script>
  <script src="assets/vendor/parallax/parallax.js"></script>
  <script src="assets/vendor/wow/wow.min.js"></script>
  <script src="assets/vendor/isotope-layout/isotope.pkgd.min.js"></script>
  <script src="assets/vendor/nivo-slider/js/jquery.nivo.slider.js"></script>
  <script src="assets/vendor/owl.carousel/owl.carousel.min.js"></script>
  <script src="assets/vendor/venobox/venobox.min.js"></script>

  <!-- Template Main JS File -->
  <script src="assets/js/main.js"></script>
