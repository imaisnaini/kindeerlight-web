<!-- ================================================
    * Home Website Kindeerlight
    * Written by imaisnaini
    * Created at 4th January 2021    
    ================================================ -->
<?php
  $page = 'index';
  
	include 'admin/core/init.php';
	include 'include/header.php';
	include 'include/navbar.php';
	session_start();
    if(!isset($_SESSION['login_user'])) {
      //header('location:login/index.php');
    }else {
      $login_user = $_SESSION['login_user'];
    }
?>
<body data-spy="scroll" data-target="#navbar-example">

  <!-- ======= Slider Section ======= -->
  <div id="home" class="slider-area">
    <div class="bend niceties preview-2">
      <div id="ensign-nivoslider" class="slides">
        <img src="assets/img/slider/DSC01844.jpg" alt="" title="#slider-direction-1" />
        <img src="assets/img/slider/DSC01854.jpg" alt="" title="#slider-direction-2" />
        <img src="assets/img/slider/DSC01699.jpg" alt="" title="#slider-direction-3" />
      </div>
    </div>
  </div><!-- End home slider area-->

  <!-- ====== Products Section ====== -->
  <div class="products-area area-padding">
    <div class="container">
      <div class="row">
        <div class="col-md-12 col-sm-12 col-xs-12">
          <div class="section-headline text-center">
            <h2>Our Products</h2>
          </div>
        </div>
      </div>
      <div class="row">
        <?php
          $queryProduct = "SELECT a.id, a.name, a.price, b.url FROM tbl_product a, tbl_product_pict b 
                          WHERE b.product_id = a.id AND b.isMain = 1 ORDER BY RAND() LIMIT 3";
          $resultProduct = mysqli_query($conn, $queryProduct);
          while($rowProduct = $resultProduct->fetch_assoc()){
        ?>
        <div class="col-md-4 col-sm-4 col-xs-12">
          <div class="single-team-member">
            <div class="team-img">
              <a href="#">          
                <?php echo '<img src="assets/img/products/'.$rowProduct['url'].'" alt="'.$rowProduct['url'].'">';?>
              </a>
              <div class="team-social-icon text-center">
                <ul>
                  <li>
                    <a href="#">
                      <i class="fa fa-shopping-cart"></i>
                    </a>
                  </li>
                </ul>
              </div>
            </div>
            <div class="team-content text-center">
              <h4><?php echo $rowProduct['name']; ?></h4>
              <p>Rp. <?php echo number_format($rowProduct['price']);?></p>
            </div>
          </div>
        </div>
        <?php } ?>
      </div>
    </div>
  </div>
  <!-- ====== End Products Section ====== -->

  <!-- ======= Testimonials Section ======= -->
  <div class="testimonials-area">
    <div class="testi-inner area-padding">
      <div class="testi-overly"></div>
      <div class="container ">
      <div class="row">
        <div class="col-md-12 col-sm-12 col-xs-12">
          <div class="section-headline text-center">
            <h2>Testimonials</h2>
          </div>
        </div>
      </div>
        <div class="row">
          <div class="col-md-12 col-sm-12 col-xs-12">
            <!-- Start testimonials Start -->
            <div class="testimonial-content text-center">
              <!-- start testimonial carousel -->
              <div class="owl-carousel testimonial-carousel">
                <?php
                  $queryTesti = "SELECT * FROM tbl_testimoni ORDER BY RAND() LIMIT 3";
                  $resultTesti = mysqli_query($conn, $queryTesti);
                  while($rowTesti = $resultTesti->fetch_assoc()){
                ?>
                <div class="single-testi">
                  <div class="testi-text">
                    <p>
                      <?php echo $rowTesti['testi']; ?> 
                    </p>
                    <h6>
                      <?php echo $rowTesti['name']; ?>
                    </h6>
                  </div>
                </div>
                <?php } ?>
                <!-- End single item -->
            </div>
            <!-- End testimonials end -->
          </div>
          <!-- End Right Feature -->
        </div>
      </div>
    </div>
  </div><!-- End Testimonials Section -->
</body>

<?php
	include 'include/footer.php'
?>
</html>